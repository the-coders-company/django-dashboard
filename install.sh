#!/bin/bash

virtualenv .virtualenv --python=python3
source .virtualenv/bin/activate
pip install -r requirements.txt
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata */fixtures/*.json