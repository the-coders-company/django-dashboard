from django.core.exceptions import PermissionDenied


def admin_only(func):
    def wrapper(*kwargs):
        if kwargs[0].user.is_superuser:
            return func(*kwargs)
        else:
            raise PermissionDenied

    return wrapper
