from django.urls import path
from users.views import *

app_name = "users"

urlpatterns = [
    path("", index, name="index"),
    path("list", get_users_list, name="datatable"),
    path("create", create, name="create"),
    path("<int:id>/delete", delete, name="delete"),
    path("<int:id>/edit", update, name="edit"),
]
