from django.urls import path
from settings.views import index, template_options

app_name = "settings"

urlpatterns = [
    path("", index, name="index"),
    path("template", template_options, name="customizer"),
]
